<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 24/11/2017
 * Time: 14:40
 */

$create_table_tags =  "CREATE TABLE tags (
                                `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                                `name` varchar(100) DEFAULT NULL
                            )";

$create_table_products_tags = "CREATE TABLE products_tags (
                                    id int NOT NULL AUTO_INCREMENT,
                                    product_id int,
                                    tag_id int,
                                    FOREIGN KEY(product_id) REFERENCES products(id),
                                    FOREIGN KEY(tag_id) REFERENCES tags(id),
                                    PRIMARY KEY (ID)
                                  )";

$pdo->exec($create_table_tags);
$pdo->exec($create_table_products_tags);

$tags = [
    [
        "name" => "jeux"
    ],
    [
        "name" => "television"
    ],
    [
        "name" => "interieur"
    ]
];

$create_tags_req = $pdo->prepare("INSERT INTO tags (name) VALUES (:name)");

foreach($tags as $tag) {
    $create_tags_req->execute(array("name" => $tag["name"]));
}

$db_products = $pdo->query('SELECT * FROM products');
$db_products_fetched = $db_products->fetchAll(PDO::FETCH_ASSOC);

$db_tags = $pdo->query('SELECT * FROM tags');
$db_tags_fetched = $db_tags->fetchAll(PDO::FETCH_ASSOC);

$product_tag_req = $pdo->prepare("INSERT INTO products_tags (tag_id, product_id) VALUES (:tag_id, :product_id)");

foreach($db_products_fetched as $product) {
    $rand_tag_id = rand(0, sizeof($tags)-1);
    echo $rand_tag_id;

    $product_tag_req->execute(array("tag_id" => $db_tags_fetched[$rand_tag_id]["id"], "product_id" => $product["id"]));
}

echo "Part 2 successfull";