<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 13/10/2017
 * Time: 11:58
 */

include './requests.php';

$servername = "localhost";
$username = "root";
$password = "root";

$db = "test";

try {
   // CREATE DATABASE
    $pdo = new PDO("mysql:host=localhost;", $username, $password);

    $pdo->exec("CREATE DATABASE `$db`;
                CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';
                GRANT ALL ON `$db`.* TO '$username'@'localhost';
                FLUSH PRIVILEGES;")
    or die(print_r($pdo->errorInfo(), true));

    $pdo->exec("USE `$db`");

    // $pdo = new PDO("mysql:host=localhost;dbname=$db", $username, $password);


    // CREATE TABLES
    $pdo->exec($create_table_products);
    $pdo->exec($create_table_clients);
    $pdo->exec($create_table_commands);
    $pdo->exec($create_table_phones);
    $pdo->exec($create_table_command_products);


    //IMPORT TP 1
    include './tp_1.php';
    include './tp_2.php';

} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
} catch (Exception $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

