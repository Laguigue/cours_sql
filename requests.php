<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 24/11/2017
 * Time: 12:55
 */

/**CREATE TABLE PRODUCTS**/

$create_table_products = "CREATE TABLE products (
                                id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                                name VARCHAR(100),
                                price DECIMAL(5, 2),
                                stock INT
                            )";

// $pdo->exec($create_table_products);

/**CREATE TABLE CLIENTS**/

$create_table_clients =  "CREATE TABLE clients (
                                `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                                `name` varchar(100) DEFAULT NULL
                            )";

// $pdo->exec($create_table_clients);


/**CREATE TABLE COMMANDS**/

$create_table_commands = "CREATE TABLE commands (
                                id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                                client_id INTEGER,
                                FOREIGN KEY(client_id) REFERENCES clients(id)
                            )";

// $pdo->exec($create_table_commands);


/**CREATE TABLE PHONES**/

$create_table_phones = "CREATE TABLE phone_numbers (
                                        id int NOT NULL AUTO_INCREMENT,
                                        number int,
                                        client_id int,
                                        FOREIGN KEY(client_id) REFERENCES clients(id),
                                        PRIMARY KEY (ID)
                                      )";

// $pdo->exec($create_table_phones);


/**CREATE TABLE COMMAND PRODUCTS**/

$create_table_command_products = "CREATE TABLE command_products (
                                    id int NOT NULL AUTO_INCREMENT,
                                    command_id int,
                                    product_id int,
                                    FOREIGN KEY(command_id) REFERENCES commands(id),
                                    FOREIGN KEY(product_id) REFERENCES products(id),
                                    PRIMARY KEY (ID)
                                  )";

// $pdo->exec($create_table_command_products);
