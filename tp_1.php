<?php
/**
 * Created by PhpStorm.
 * User: guillaume
 * Date: 24/11/2017
 * Time: 14:40
 */

/*** Create clients ***/

$clients = [
    [
        "name" => "Laguigue"
    ],
    [
        "name" => "Simon say"
    ],
    [
        "name" => "Bremy"
    ]
];


$req = $pdo->prepare("INSERT INTO clients (name) VALUES (:name)");

foreach($clients as $client) {
    $req->execute(array(":name" => $client["name"]));
}



//Create products
$products = [
    [
        "name" => "Chair",
        "price" => 3.45,
        "stock" => 12
    ],
    [
        "name" => "Tabloids",
        "price" => 10.45,
        "stock" => 2
    ],
    [
        "name" => "Coca",
        "price" => 20.45,
        "stock" => 0
    ]
];

$product_req = $pdo->prepare("INSERT INTO products (name, price, stock) VALUES (:name, :price, :stock)");

foreach($products as $product) {
    $product_req->execute(array("name" => $product["name"], "price" => $product["price"], "stock" => $product["stock"]));
}

/*** Create commands ***/

$clients = $pdo->query('SELECT * FROM clients');
$clientsFetched = $clients->fetchAll(PDO::FETCH_ASSOC);

$command_req = $pdo->prepare("INSERT INTO commands (client_id) VALUES (:client_id)");

foreach($clientsFetched as $client) {
    $command_req->execute(array("client_id" => $client["id"]));
}

/*** Create phones numbers ***/

$phones_req = $pdo->prepare("INSERT INTO phone_numbers (number, client_id) VALUES (:number, :client_id)");

foreach($clientsFetched as $client) {
    $randnum = rand(1111111111,9999999999);
    $phones_req->execute(array("number" => $randnum, "client_id" => $client["id"]));
}

/*** Create commands_products ***/
$commands = $pdo->query('SELECT * FROM commands');

$db_products = $pdo->query('SELECT * FROM products');
$db_products_fetched = $db_products->fetchAll(PDO::FETCH_ASSOC);
$products = [];
foreach($db_products_fetched as $product) {
    $products[] = $product;
}

$command_product_req = $pdo->prepare("INSERT INTO command_products (command_id, product_id) VALUES (:command_id, :product_id)");
$commands_fetched = $commands->fetchAll(PDO::FETCH_ASSOC);

foreach($commands_fetched as $command) {
    $rand_iteration = rand(0, 3);

    for ($i = 0; $i < $rand_iteration; $i++) {
        $rand_product_id = rand(0, sizeof($products)-1);
        $command_product_req->execute(array("command_id" => $command["id"], "product_id" => $products[$rand_product_id]["id"]));
    }
}

/** Add signin_at */
$pdo->exec("ALTER TABLE clients ADD COLUMN (signin_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
$pdo->exec("ALTER TABLE clients CHANGE signin_at signup_at TIMESTAMP ");

echo "Part 1 successfull";